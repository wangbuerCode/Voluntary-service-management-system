package com.tomasyao.io.user.controller;

import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.base.vo.ResponseMap;
import com.tomasyao.io.user.model.Order;
import com.tomasyao.io.user.model.Scheme;
import com.tomasyao.io.user.model.User;
import com.tomasyao.io.user.service.IOrderService;
import com.tomasyao.io.user.service.ISchemeService;
import com.tomasyao.io.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping(value = "/order")
public class OrderController {
    @Autowired
    private IOrderService orderService;
    @Autowired
    private IUserService userService;
    @Autowired
    private ISchemeService schemeService;

    @RequestMapping(value = "/addOneOrder", method = RequestMethod.POST)
    public Map addOneOrder(HttpServletRequest request, int userId, Integer schemeId, String status) {
        ResponseMap map = ResponseMap.getInstance();
        Order order = null;
        User user = null;
        Scheme scheme = null;

        try {
            if(orderService.getOneOrder(userId,schemeId) != null){
                return map.putFailure("该用户已预订过改服务安排！！！", 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            user = userService.getOneUser(userId);
            scheme = schemeService.getOneScheme(schemeId);
            if (user == null) {
                return map.putFailure("用户不存在，不能进行预定！！！", 101);
            } else if (scheme == null) {
                return map.putFailure("志愿安排不存在，不能进行预定！！！", 101);
            } else {
                order = orderService.addOneOrder(user, scheme, new Date(), status);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(order, "添加成功");
    }

    @RequestMapping(value = "/getOneOrder", method = RequestMethod.POST)
    public Map getOneOrder(HttpServletRequest request, int id) {
        ResponseMap map = ResponseMap.getInstance();
        Order order = null;
        try {
            order = orderService.getOneOrder(id);
            if (order == null) {
                return map.putFailure("获取失败", 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(order);
    }

    /*@RequestMapping(value = "/getOneOrderTwo", method = RequestMethod.POST)
    public Map getOneOrderTwo(int userId, int schemeId) {
        ResponseMap map = ResponseMap.getInstance();
        Order order = null;
        try {
            order = orderService.getOneOrder(userId,schemeId);
            if (order == null) {
                return map.putFailure("获取失败", 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(order);
    }*/

    @RequestMapping(value = "/deleteOneOrder", method = RequestMethod.POST)
    public Map deleteOneOrder(HttpServletRequest request, int id) {
        ResponseMap map = ResponseMap.getInstance();
        try {
            orderService.deleteOneOrder(id);
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putSuccess("删除成功");
    }

    @RequestMapping(value = "/getOrderPageList", method = RequestMethod.POST)
    public Map getOrderPageList(HttpServletRequest request, int current, int size, String search,Integer userId) {
        ResponseMap map = ResponseMap.getInstance();
        Page<Order> orderPage;
        try {
            orderPage = orderService.getOrderPageList(current, size, search,userId);
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putPage(orderPage);
    }

    @RequestMapping(value = "/updateOneOrder", method = RequestMethod.POST)
    public Map updateOneOrder(HttpServletRequest request, int id, int userId, Integer schemeId, String status) {
        ResponseMap map = ResponseMap.getInstance();
        Order order = null;
        User user = null;
        Scheme scheme = null;

        try {
            user = userService.getOneUser(userId);
            scheme = schemeService.getOneScheme(schemeId);
            if (user == null) {
                return map.putFailure("用户不存在，不能进行预定！！！", 101);
            } else if (scheme == null) {
                return map.putFailure("志愿安排不存在，不能进行预定！！！", 101);
            } else {
                order = orderService.updateOneOrder(id, user, scheme, status);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(order, "修改成功");
    }

}
