package com.tomasyao.io.user.controller;

import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.base.vo.ResponseMap;
import com.tomasyao.io.user.model.Scheme;
import com.tomasyao.io.user.model.User;
import com.tomasyao.io.user.service.ISchemeService;
import com.tomasyao.io.user.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@RestController
@RequestMapping(value = "/scheme")
public class SchemeController {
    @Autowired
    private ISchemeService schemeService;
    @Autowired
    private IUserService userService;

    @RequestMapping(value = "/addOneScheme", method = RequestMethod.POST)
    public Map addOneUser(HttpServletRequest request, int userId, String content, String startTime, String endTime, String status) {
        ResponseMap map = ResponseMap.getInstance();
        Scheme scheme = null;
        User user = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startDate = sdf.parse(startTime);
            Date endDate = sdf.parse(endTime);

            user = userService.getOneUser(userId);
            if (user == null) {
                return map.putFailure("该用户不存在，不能进行志愿安排！！！", 101);
            } else{
                scheme = schemeService.addOneScheme(user, content, startDate, endDate, status);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(scheme, "添加成功");
    }

    @RequestMapping(value = "/getOneScheme", method = RequestMethod.POST)
    public Map getOneScheme(HttpServletRequest request, int id) {
        ResponseMap map = ResponseMap.getInstance();
        Scheme scheme = null;
        try {
            scheme = schemeService.getOneScheme(id);
            if (scheme == null) {
                return map.putFailure("获取失败", 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(scheme);
    }

    @RequestMapping(value = "/deleteOneScheme", method = RequestMethod.POST)
    public Map deleteOneScheme(HttpServletRequest request, int id) {
        ResponseMap map = ResponseMap.getInstance();
        try {
            schemeService.deleteOneScheme(id);
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putSuccess("删除成功");
    }

    @RequestMapping(value = "/getSchemePageList", method = RequestMethod.POST)
    public Map getSchemePageList(HttpServletRequest request, int current, int size, String search) {
        ResponseMap map = ResponseMap.getInstance();
        Page<Scheme> schemePage;
        try {
            schemePage = schemeService.getSchemePageList(current, size, search);
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putPage(schemePage);
    }

    @RequestMapping(value = "/updateOneScheme", method = RequestMethod.POST)
    public Map updateOneScheme(HttpServletRequest request, int id, int userId, String content, String startTime, String endTime, String status) {
        ResponseMap map = ResponseMap.getInstance();
        Scheme scheme = null;
        User user = null;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date startDate = sdf.parse(startTime);
            Date endDate = sdf.parse(endTime);

            user = userService.getOneUser(userId);
            if (user == null) {
                return map.putFailure("该用户不存在，不能进行志愿安排！！！", 101);
            } else{
                scheme = schemeService.updateOneScheme(id, user, content, startDate, endDate, status);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(scheme, "修改成功");
    }

}
