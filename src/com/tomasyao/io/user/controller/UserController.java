package com.tomasyao.io.user.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.base.vo.ResponseMap;
import com.tomasyao.io.user.model.User;
import com.tomasyao.io.user.service.IOrderService;
import com.tomasyao.io.user.service.ISchemeService;
import com.tomasyao.io.user.service.IUserService;
import com.tomasyao.io.user.vo.UserVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private IUserService userService;
    @Autowired
    private IOrderService orderService;
    @Autowired
    private ISchemeService schemeService;

    @RequestMapping(value = "/addOneUser", method = RequestMethod.POST)
    public Map addOneUser(HttpServletRequest request, String username, String password, String avatar, String role, String tel, String email) {
        ResponseMap map = ResponseMap.getInstance();
        User user = null;
        try {
            user = userService.addOneUser(username, password, avatar, role, tel, email);
            if (user == null) {
                return map.putFailure("该用户名已存在", 100);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(user, "注册成功");
    }

    @RequestMapping(value = "/getOneUser", method = RequestMethod.POST)
    public Map getOneUser(HttpServletRequest request, int id) {
        ResponseMap map = ResponseMap.getInstance();
        User user = null;
        try {
            user = userService.getOneUser(id);
            if (user == null) {
                return map.putFailure("获取用户信息失败", 101);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(user);
    }

    @RequestMapping(value = "/deleteOneUser", method = RequestMethod.POST)
    public Map deleteOneUser(HttpServletRequest request, int id) {
        ResponseMap map = ResponseMap.getInstance();
        try {
              // git reset --hard

//            List<Scheme> schemelist = schemeService.findSchemeListByUserId(id);
//            List<Order> orderlist = orderService.findOrderListByUserId(id);
//            if (schemelist != null) {
//                schemeService.deleteSchemeByUserId(id);
//            }
//            if (orderlist != null) {
//                orderService.deleteOrderByUserId(id);
//            }

            userService.deleteOneUser(id);

        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putSuccess("删除成功");
    }

    @RequestMapping(value = "/getUserPageList", method = RequestMethod.POST)
    public Map getUserPageList(HttpServletRequest request, int current, int size, String search, String roleType) {
        ResponseMap map = ResponseMap.getInstance();
        Page<User> userPage;
        try {
            userPage = userService.getUserPageList(current, size, search, roleType);
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putPage(userPage);
    }

    @RequestMapping(value = "/updateOneUser", method = RequestMethod.POST)
    public Map updateOneUser(HttpServletRequest request, int id, String username, String avatar, String tel, String email) {
        ResponseMap map = ResponseMap.getInstance();
        User user = null;
        try {
            user = userService.updateOneUser(id, username, avatar, tel, email);
            if (user == null) {
                return map.putFailure("该用户名已存在", 100);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putValue(user, "修改成功");
    }


    @RequestMapping(value = "/updatePwdByOld", method = RequestMethod.POST)
    public Map updatePwdByOld(HttpServletRequest request, int id, String oldPwd, String newPwd) {
        ResponseMap map = ResponseMap.getInstance();
        try {
            User user = userService.updatePwdByOld(id, oldPwd, newPwd);
            if (user == null) {
                return map.putFailure("旧密码不正确", 103);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putSuccess("操作成功");
    }

    @RequestMapping(value = "/updatePwdByAdmin", method = RequestMethod.POST)
    public Map updatePwdByAdmin(HttpServletRequest request, int id, String newPwd) {
        ResponseMap map = ResponseMap.getInstance();
        try {
            userService.updatePwdByAdmin(id, newPwd);
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("交互失败", 1);
        }

        return map.putSuccess("操作成功");
    }

    @RequestMapping(value = "/loginApp", method = RequestMethod.POST)
    public Map loginApp(HttpServletRequest request, HttpServletResponse response, String username, String password) {
        ResponseMap map = ResponseMap.getInstance();
        UserVo userVo = null;
        try {
            userVo = userService.loginApp(username, password);
            if (!userVo.isSuccess()) {
                return map.putFailure(userVo.getMessage(), 108);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("Interaction failed", 1);
        }

        Cookie cookie = null;
        try {
            cookie = new Cookie("user", new ObjectMapper().writeValueAsString(userVo.getUser()));
            cookie.setPath("/");
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        response.addCookie(cookie);
        return map.putValue(userVo.getUser(), "success");
    }

    @RequestMapping(value = "/loginAdmin", method = RequestMethod.POST)
    public Map loginAdmin(HttpServletRequest request, HttpServletResponse response, String username, String password) {
        ResponseMap map = ResponseMap.getInstance();
        String token = UUID.randomUUID().toString();
        try {
            UserVo result = userService.loginAdmin(token, username, password);
            if (result != null) {
                Cookie cookie = new Cookie("token", token);
                cookie.setPath("/");
                response.addCookie(cookie);
                return map.putValue(result, "Administrator login succeeded");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return map.putFailure("Interaction failed", 1);
        }

        return map.putFailure("Wrong user name or password", 105);
    }
}
