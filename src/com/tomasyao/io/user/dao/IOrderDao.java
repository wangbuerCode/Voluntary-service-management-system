package com.tomasyao.io.user.dao;

import com.tomasyao.io.base.dao.IBaseDao;
import com.tomasyao.io.user.model.Order;

public interface IOrderDao extends IBaseDao<Order> {
}
