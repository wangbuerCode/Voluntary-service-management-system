package com.tomasyao.io.user.dao;

import com.tomasyao.io.base.dao.IBaseDao;
import com.tomasyao.io.user.model.Scheme;

public interface ISchemeDao extends IBaseDao<Scheme> {
}
