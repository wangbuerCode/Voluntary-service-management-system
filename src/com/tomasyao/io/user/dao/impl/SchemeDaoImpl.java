package com.tomasyao.io.user.dao.impl;

import com.tomasyao.io.base.dao.impl.BaseDaoImpl;
import com.tomasyao.io.user.dao.ISchemeDao;
import com.tomasyao.io.user.model.Scheme;
import org.springframework.stereotype.Component;

@Component
public class SchemeDaoImpl extends BaseDaoImpl<Scheme> implements ISchemeDao {
    public SchemeDaoImpl() {
        super(Scheme.class);
    }
}
