package com.tomasyao.io.user.dao.impl;

import com.tomasyao.io.base.dao.impl.BaseDaoImpl;
import com.tomasyao.io.user.dao.IOrderDao;
import com.tomasyao.io.user.model.Order;
import org.springframework.stereotype.Component;

@Component
public class OrderDaoImpl extends BaseDaoImpl<Order> implements IOrderDao {
    public OrderDaoImpl() {
        super(Order.class);
    }
}
