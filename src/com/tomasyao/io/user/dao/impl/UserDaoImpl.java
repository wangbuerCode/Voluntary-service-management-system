package com.tomasyao.io.user.dao.impl;

import com.tomasyao.io.base.dao.impl.BaseDaoImpl;
import com.tomasyao.io.user.dao.IUserDao;
import com.tomasyao.io.user.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserDaoImpl extends BaseDaoImpl<User> implements IUserDao {
    public UserDaoImpl() {
        super(User.class);
    }
}
