package com.tomasyao.io.user.service;

import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.user.model.Order;
import com.tomasyao.io.user.model.Scheme;
import com.tomasyao.io.user.model.User;

import java.util.Date;
import java.util.List;

public interface IOrderService {

    //新增预定
    Order addOneOrder(User userId, Scheme schemeId, Date createTime, String status) throws Exception;

    //获取一个预定
    Order getOneOrder(int id) throws Exception;

    //获取一个预定
    Order getOneOrder(int userId,int schemeId) throws Exception;

    //删除一个预定
    boolean deleteOneOrder(int id) throws Exception;

    //获取安排预定
    Page<Order> getOrderPageList(int current, int size, String search,Integer userId) throws Exception;

    //编辑预定
    Order updateOneOrder(int id, User userId, Scheme schemeId, String status) throws Exception;

    // 根据userId获取对应的安排
    List<Order> findOrderListByUserId(int userId) throws Exception;

    // 根据userId删除对应的安排
    boolean deleteOrderByUserId(int userId) throws Exception;
}
