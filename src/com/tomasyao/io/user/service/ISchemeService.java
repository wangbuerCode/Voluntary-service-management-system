package com.tomasyao.io.user.service;

import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.user.model.Scheme;
import com.tomasyao.io.user.model.User;

import java.util.Date;
import java.util.List;

public interface ISchemeService {

    //新增安排
    Scheme addOneScheme(User userId, String content, Date startTime, Date endTime, String status) throws Exception;

    //获取一个安排
    Scheme getOneScheme(int id) throws Exception;

    //删除一个安排
    boolean deleteOneScheme(int id) throws Exception;

    //获取安排列表
    Page<Scheme> getSchemePageList(int current, int size, String search) throws Exception;

    //编辑安排
    Scheme updateOneScheme(int id, User userId, String content, Date startTime, Date endTime, String status) throws Exception;

    // 根据userId获取对应的安排
    List<Scheme> findSchemeListByUserId(int userId) throws Exception;

    // 根据userId删除对应的安排
    boolean deleteSchemeByUserId(int userId) throws Exception;
}
