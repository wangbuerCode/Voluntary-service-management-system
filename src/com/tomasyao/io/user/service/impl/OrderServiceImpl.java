package com.tomasyao.io.user.service.impl;

import com.tomasyao.io.base.util.HqlUtil;
import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.base.vo.Parameter;
import com.tomasyao.io.user.dao.IOrderDao;
import com.tomasyao.io.user.model.Order;
import com.tomasyao.io.user.model.Scheme;
import com.tomasyao.io.user.model.User;
import com.tomasyao.io.user.service.IOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class OrderServiceImpl implements IOrderService {
    @Autowired
    private IOrderDao orderDao;

    @Override
    public Order addOneOrder(User userId, Scheme schemeId, Date createTime, String status) throws Exception {
        Order order = new Order(userId, schemeId, createTime, " ");
        orderDao.save(order);
        return order;
    }

    @Override
    public Order getOneOrder(int id) throws Exception {
        return orderDao.findOne(" from Order o where o.id='" + id + "' ");
    }

    @Override
    public Order getOneOrder(int userId, int schemeId) throws Exception {
        return orderDao.findOne(" from Order o where o.userId.id='"+userId+"' " +
                " and o.schemeId.id='"+schemeId+"' ");
    }

    @Override
    public boolean deleteOneOrder(int id) throws Exception {
        orderDao.deleteWithHql(" delete from Order o where o.id='" + id + "' ");
        return true;
    }

    @Override
    public Page<Order> getOrderPageList(int current, int size, String search,Integer userId) throws Exception {
        String hql = " from Order o ";
        String countHql = " select count(*) from Order o ";
        Parameter p = new Parameter();
        List<String> condition = new ArrayList<String>();

        condition.add(" o.status like :search ");
        p.put("search", "%" + search + "%");

        if(userId != null){
            condition.add(" o.userId.id = :userId ");
            p.put("userId", userId);
        }

        return orderDao.findPage(current, size, HqlUtil.formatHql(hql, condition, "o.id", false),
                HqlUtil.formatHql(countHql, condition), p);
    }

    @Override
    public Order updateOneOrder(int id, User userId, Scheme schemeId, String status) throws Exception {
        Order order = orderDao.getOne(id);

        order.setUserId(userId);
        order.setSchemeId(schemeId);
        order.setStatus(status);

        return orderDao.update(order);
    }

    @Override
    public List<Order> findOrderListByUserId(int userId) throws Exception {
        return orderDao.findList(" from Order o where o.userId.id='" + userId + "' ");
    }

    @Override
    public boolean deleteOrderByUserId(int userId) throws Exception {
        orderDao.deleteWithHql(" delete from Order o where o.userId.id='" + userId + "' ");
        return true;
    }
}
