package com.tomasyao.io.user.service.impl;

import com.tomasyao.io.base.util.HqlUtil;
import com.tomasyao.io.base.vo.Page;
import com.tomasyao.io.base.vo.Parameter;
import com.tomasyao.io.user.dao.ISchemeDao;
import com.tomasyao.io.user.dao.IUserDao;
import com.tomasyao.io.user.model.Scheme;
import com.tomasyao.io.user.model.User;
import com.tomasyao.io.user.service.ISchemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SchemeServiceImpl implements ISchemeService {
    @Autowired
    private ISchemeDao schemeDao;

    @Override
    public Scheme addOneScheme(User userId, String content, Date startTime, Date endTime, String status) throws Exception {

        Scheme scheme = new Scheme(userId, content, startTime, endTime, status);
        schemeDao.save(scheme);
        return scheme;
    }

    @Override
    public Scheme getOneScheme(int id) throws Exception {
        return schemeDao.findOne(" from Scheme s where s.id='" + id + "' ");
    }

    @Override
    public boolean deleteOneScheme(int id) throws Exception {
        schemeDao.deleteWithHql(" delete from Scheme s where s.id='" + id + "' ");
        return true;
    }

    @Override
    public Page<Scheme> getSchemePageList(int current, int size, String search) throws Exception {
        String hql = " from Scheme s ";
        String countHql = " select count(*) from Scheme s ";
        Parameter p = new Parameter();
        List<String> condition = new ArrayList<String>();

        condition.add(" (s.content like :search or s.status like :search) ");
        p.put("search", "%" + search + "%");

        return schemeDao.findPage(current, size, HqlUtil.formatHql(hql, condition, "s.id", false),
                HqlUtil.formatHql(countHql, condition), p);
    }

    @Override
    public Scheme updateOneScheme(int id, User userId, String content, Date startTime, Date endTime, String status) throws Exception {
        Scheme scheme = schemeDao.getOne(id);

        //scheme.setUserId(userId);
        scheme.setContent(content);
        scheme.setStartTime(startTime);
        scheme.setEndTime(endTime);
        //scheme.setStatus(status);

        return schemeDao.update(scheme);
    }

    @Override
    public List<Scheme> findSchemeListByUserId(int userId) throws Exception {
        return schemeDao.findList(" from Scheme s where s.userId.id='" + userId + "' ");
    }

    @Override
    public boolean deleteSchemeByUserId(int userId) throws Exception {
        schemeDao.deleteWithHql(" delete from Scheme s where s.userId.id='" + userId + "' ");
        return true;
    }
}
