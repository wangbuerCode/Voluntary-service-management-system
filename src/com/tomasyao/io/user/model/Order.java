package com.tomasyao.io.user.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "tb_order")
public class Order {
    @Id
    @GeneratedValue
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;
    @ManyToOne
    @JoinColumn(name = "scheme_id")
    private Scheme schemeId;
    @Column(name = "create_time")
    private Date createTime;
    private String status;

    public Order() {
    }

    public Order(User userId, Scheme schemeId, Date createTime, String status) {
        this.userId = userId;
        this.schemeId = schemeId;
        this.createTime = createTime;
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUserId() {
        return userId;
    }

    public void setUserId(User userId) {
        this.userId = userId;
    }

    public Scheme getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(Scheme schemeId) {
        this.schemeId = schemeId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}